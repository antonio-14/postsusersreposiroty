package com.project.postsusers.ModulAllUsers

import com.project.postsusers.Model.Room.UserEntity
import com.project.postsusers.ModulAllUsers.AllUsersInterfaces.*
import com.project.postsusers.Utils.vo.Resource

class AllUsersRepo(private val dataSource: AllUsersDataSource): AllUserRepoI {
    override suspend fun getAllUsers(): Resource.Success<MutableList<UserEntity>> = dataSource.getAllUsers()
}