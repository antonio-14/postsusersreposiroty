package com.project.postsusers.ModulAllUsers.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import androidx.recyclerview.widget.RecyclerView
import com.project.postsusers.Model.Room.UserEntity
import com.project.postsusers.ModulAllUsers.AllUsersInterfaces.*
import com.project.postsusers.databinding.ItemUserBinding
import java.util.*

class UsersAdapter(private val listener: UserInterface): RecyclerView.Adapter<UsersAdapter.UserViewHolder>() {

    private val usersList: MutableList<UserEntity> = mutableListOf()
    var usersListFilter: MutableList<UserEntity> = mutableListOf()

    fun addData(newData: List<UserEntity>) {
        usersList.addAll(newData)
        usersListFilter = usersList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            UserViewHolder(ItemUserBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun getItemCount() = usersListFilter.size

    override fun getItemId(position: Int): Long = usersListFilter[position].id.toLong()

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(usersListFilter[position])
    }

    inner class UserViewHolder(private val binding: ItemUserBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: UserEntity) {
            binding.apply {
                tvName.text = item.name
                tvPhone.text = item.phone
                tvEmail.text = item.email
                tvSeePublications.setOnClickListener {
                    listener.goToUser(item)
                }
            }
        }
    }

    fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString().lowercase(Locale.US)
                usersListFilter = if (charString.isEmpty()) {
                    usersList
                } else {
                    val filteredList = mutableListOf<UserEntity>()
                    for (user in usersList) {
                        if (user.name?.lowercase(Locale.US)?.contains(charString) == true ||
                            user.phone?.lowercase(Locale.US)?.contains(charString) == true
                            || user.email?.lowercase(Locale.US)?.contains(charString) == true) {
                            filteredList.add(user)
                        }
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = usersListFilter
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                usersListFilter = filterResults.values as MutableList<UserEntity>
                if (usersListFilter.isEmpty()) {
                    listener.noResults(true)
                } else {
                    listener.noResults(false)
                }
                notifyDataSetChanged()
            }
        }
    }
}