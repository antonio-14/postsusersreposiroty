package com.project.postsusers.ModulAllUsers

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.project.postsusers.Utils.vo.Resource
import kotlinx.coroutines.Dispatchers

class AllUsersViewModel(private val repo: AllUsersRepo): ViewModel() {

    fun getAllUsers() = liveData(viewModelScope.coroutineContext + Dispatchers.Main) {
        emit(Resource.Loading())
        try {
            emit(repo.getAllUsers())
        } catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }

}

class AllUsersViewModelFactory(private val repo: AllUsersRepo) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(AllUsersRepo::class.java).newInstance(repo)
    }
}