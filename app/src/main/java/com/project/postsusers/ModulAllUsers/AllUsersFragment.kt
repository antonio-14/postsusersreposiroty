package com.project.postsusers.ModulAllUsers

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.project.postsusers.Model.Room.UserEntity
import com.project.postsusers.ModulAllUsers.AllUsersInterfaces.*
import com.project.postsusers.ModulAllUsers.adapter.UsersAdapter
import com.project.postsusers.R
import com.project.postsusers.Utils.DB.AppDatabase
import com.project.postsusers.Utils.Extensions.hide
import com.project.postsusers.Utils.Extensions.show
import com.project.postsusers.Utils.vo.Resource
import com.project.postsusers.databinding.FragmentAllUsersBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AllUsersFragment : Fragment(R.layout.fragment_all_users), UserInterface {

    private lateinit var binding: FragmentAllUsersBinding
    private lateinit var adapter: UsersAdapter
    private var listUserEntity: MutableList<UserEntity>? = null

    private val viewModelAllUsers by viewModels<AllUsersViewModel> { AllUsersViewModelFactory(
        AllUsersRepo(AllUsersDataSource(requireContext(),  AppDatabase.getDatabase(requireActivity().applicationContext)))
    ) }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView(view)
        hideToolbar()
        setupRecyclerView()
        getAllUsers()
        setupSearch()
    }

    private fun hideToolbar() {
        (activity as AppCompatActivity?)?.supportActionBar?.hide()
    }

    private fun setupSearch() {
        binding.searchViewUsers.setOnClickListener {
            binding.searchViewUsers.isIconified = false
        }

        binding.searchViewUsers.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                binding.rvAllUsers.adapter?.run {
                    (binding.rvAllUsers.adapter as UsersAdapter).getFilter().filter(query)
                }
                return false
            }
            override fun onQueryTextChange(newText: String): Boolean {
                binding.rvAllUsers.adapter?.run {
                    (binding.rvAllUsers.adapter as UsersAdapter).getFilter().filter(newText)
                }
                return true
            }
        })
    }

    private fun getAllUsers() {
        viewModelAllUsers.getAllUsers().observe(viewLifecycleOwner) { result ->
            when (result) {
                is Resource.Loading -> {
                    showProgressBar()
                }
                is Resource.Success -> {
                    hideProgressBar()
                    listUserEntity = result.data
                    if (result.data.isEmpty()) {
                        binding.noResults.show()
                    } else {
                        binding.noResults.hide()
                        adapter.addData(result.data)
                        binding.rvAllUsers.adapter = adapter
                    }
                }
                is Resource.Failure -> {
                    hideProgressBar()
                    binding.noResults.show()
                }
            }
        }
    }

    private fun hideProgressBar() {
        lifecycleScope.launch(Dispatchers.Main) {
            binding.progressBarAllUsers.hide()
        }
    }

    private fun showProgressBar() {
        lifecycleScope.launch(Dispatchers.Main) {
            binding.progressBarAllUsers.show()
        }
    }

    private fun setupView(view: View) {
        binding = FragmentAllUsersBinding.bind(view)
    }

    private fun setupRecyclerView() {
        binding.rvAllUsers.layoutManager = LinearLayoutManager(requireContext())
        binding.rvAllUsers.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        adapter = UsersAdapter(this)
    }

    override fun goToUser(user: UserEntity) {
        val bundle = bundleOf("userId" to user.id)
        findNavController().navigate(R.id.action_allUsersFragment_to_allPostsDialogFragment, bundle)
    }

    override fun noResults(visibility: Boolean) {
        if (visibility) binding.noResults.show()
        else binding.noResults.hide()
    }

    companion object {
        @JvmStatic
        fun newInstance() = AllUsersFragment()
    }

}