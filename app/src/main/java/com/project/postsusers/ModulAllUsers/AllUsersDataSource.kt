package com.project.postsusers.ModulAllUsers

import android.content.Context
import com.project.postsusers.Model.Room.UserEntity
import com.project.postsusers.ModulAllUsers.AllUsersInterfaces.*
import com.project.postsusers.Utils.DB.AppDatabase
import com.project.postsusers.Utils.Extensions.mapperUserEntity
import com.project.postsusers.Utils.vo.Resource
import com.project.postsusers.Utils.vo.RetrofitClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AllUsersDataSource(private val context: Context, private val appDatabase: AppDatabase): AllUserDataSourceI {
    override suspend fun getAllUsers(): Resource.Success<MutableList<UserEntity>> {
        var allUsersEntity = mutableListOf<UserEntity>()
        withContext(Dispatchers.IO) {
            if (appDatabase.userDao().getAllUsers().isEmpty()) {
                val allUsers = RetrofitClient.webService.getAllUsers()
                for (user in allUsers) {
                    val userEntity = user.mapperUserEntity()
                    allUsersEntity.add(userEntity)
                }
                appDatabase.userDao().insertAllUsers(allUsersEntity)
            } else {
                allUsersEntity = appDatabase.userDao().getAllUsers()
            }
        }
        return Resource.Success(allUsersEntity)
    }
}