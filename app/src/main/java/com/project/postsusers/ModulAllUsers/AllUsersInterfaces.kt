package com.project.postsusers.ModulAllUsers

import com.project.postsusers.Model.Room.UserEntity
import com.project.postsusers.Utils.vo.Resource

class AllUsersInterfaces {

    interface AllUserDataSourceI {
        suspend fun getAllUsers(): Resource.Success<MutableList<UserEntity>>
    }

    interface AllUserRepoI {
        suspend fun getAllUsers(): Resource.Success<MutableList<UserEntity>>
    }

    interface UserInterface {
        fun goToUser(user: UserEntity)
        fun noResults(visibility: Boolean)
    }

}