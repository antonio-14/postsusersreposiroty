package com.project.postsusers.ModulAllPosts

import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.project.postsusers.ModulAllPosts.adapter.PostsAdapter
import com.project.postsusers.R
import com.project.postsusers.Utils.DB.AppDatabase
import com.project.postsusers.Utils.Extensions.hide
import com.project.postsusers.Utils.Extensions.show
import com.project.postsusers.Utils.vo.Resource
import com.project.postsusers.databinding.FragmentAllPostsBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AllPostsDialogFragment: BottomSheetDialogFragment() {

    private var mBehavior: BottomSheetBehavior<View>? = null
    private lateinit var binding: FragmentAllPostsBinding
    private lateinit var adapter: PostsAdapter

    private val viewModelAllPosts by viewModels<AllPostsViewModel> { AllPostsViewModelFactory(
        AllPostsRepo(AllPostsDataSource(requireContext(),  AppDatabase.getDatabase(requireActivity().applicationContext)))
    ) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentAllPostsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFullScreen()
        setListeners()
        setupRecyclerView()
        mBehavior = BottomSheetBehavior.from(view.parent as View)
        getUser()
        getAllPosts()
    }

    override fun onStart() {
        super.onStart()
        mBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun setupFullScreen() {
        with(binding) {
            val params = clBottomSheet.layoutParams
            params.height = getScreenHeight()
            clBottomSheet.layoutParams = params
        }
    }

    private fun setListeners() {
        with(binding) {
            ivClose.setOnClickListener {
                dismiss()
            }
        }
    }

    private fun getScreenHeight(): Int = Resources.getSystem().displayMetrics.heightPixels - 40

    private fun getUser() {
        viewModelAllPosts.getUSer(arguments?.getInt("userId")?:0).observe(viewLifecycleOwner) { result ->
            when (result) {
                is Resource.Loading -> {}
                is Resource.Success -> {
                    val user = result.data
                    with(binding) {
                        tvName.text = user.name
                        tvEmail.text = user.email
                        tvPhone.text = user.phone
                    }
                }
                is Resource.Failure -> {}
            }
        }
    }

    private fun getAllPosts() {
        viewModelAllPosts.getAllPostsByUser(arguments?.getInt("userId")?:0).observe(viewLifecycleOwner) { result ->
            when (result) {
                is Resource.Loading -> { showProgressBar() }
                is Resource.Success -> {
                    hideProgressBar()
                    if (result.data.isEmpty()) {
                        binding.noResults.show()
                    } else {
                        binding.noResults.hide()
                        adapter.addData(result.data)
                        binding.rvAllPosts.adapter = adapter
                    }
                }
                is Resource.Failure -> {
                    hideProgressBar()
                    binding.noResults.show()
                }
            }
        }
    }

    private fun hideProgressBar() {
        lifecycleScope.launch(Dispatchers.Main) {
            binding.progressBarAllPosts.hide()
        }
    }

    private fun showProgressBar() {
        lifecycleScope.launch(Dispatchers.Main) {
            binding.progressBarAllPosts.show()
        }
    }

    private fun setupRecyclerView() {
        binding.rvAllPosts.layoutManager = LinearLayoutManager(requireContext())
        binding.rvAllPosts.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        adapter = PostsAdapter(requireContext())
    }

}