package com.project.postsusers.ModulAllPosts.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.project.postsusers.Model.Room.PostEntity
import com.project.postsusers.R
import com.project.postsusers.databinding.ItemPostBinding

class PostsAdapter(private val context: Context): RecyclerView.Adapter<PostsAdapter.PostViewHolder>() {

    private val postsList: MutableList<PostEntity> = mutableListOf()

    fun addData(newData: List<PostEntity>) {
        postsList.addAll(newData)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PostViewHolder(ItemPostBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun getItemCount() = postsList.size

    override fun getItemId(position: Int): Long = postsList[position].id.toLong()

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(postsList[position])
    }

    inner class PostViewHolder(private val binding: ItemPostBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: PostEntity) {
            binding.apply {
                tvTitle.text = context.getString(R.string.title).replace("@title", item.title?:"")
                tvMessage.text = context.getString(R.string.message).replace("@body", item.body?:"")
            }
        }
    }
}