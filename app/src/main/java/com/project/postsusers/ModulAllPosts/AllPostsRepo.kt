package com.project.postsusers.ModulAllPosts

import com.project.postsusers.Model.Room.PostEntity
import com.project.postsusers.Model.Room.UserEntity
import com.project.postsusers.ModulAllPosts.AllPostsInterfaces.*
import com.project.postsusers.Utils.vo.Resource

class AllPostsRepo(private val dataSource: AllPostsDataSource): AllPostsRepoI {

    override suspend fun getAllPostsByUser(userId: Int): Resource.Success<MutableList<PostEntity>> = dataSource.getAllPostsByUser(userId)

    override suspend fun getUser(userId: Int): Resource.Success<UserEntity> = dataSource.getUser(userId)

}