package com.project.postsusers.ModulAllPosts

import android.content.Context
import com.project.postsusers.Model.Post
import com.project.postsusers.Model.Room.PostEntity
import com.project.postsusers.Model.Room.UserEntity
import com.project.postsusers.ModulAllPosts.AllPostsInterfaces.*
import com.project.postsusers.Utils.DB.AppDatabase
import com.project.postsusers.Utils.Extensions.mapperPostEntity
import com.project.postsusers.Utils.Extensions.mapperUserEntity
import com.project.postsusers.Utils.vo.Resource
import com.project.postsusers.Utils.vo.RetrofitClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AllPostsDataSource(private val context: Context, private val appDatabase: AppDatabase): AllPostsDataSourceI {
    override suspend fun getAllPostsByUser(userId: Int): Resource.Success<MutableList<PostEntity>> {
        var allPostsEntity = mutableListOf<PostEntity>()
        withContext(Dispatchers.IO) {
            if (appDatabase.userDao().getPostsById(userId).isEmpty()) {
                val allPosts = RetrofitClient.webService.getPostsForUserId(userId)
                for (post in allPosts) {
                    val postEntity = post.mapperPostEntity()
                    allPostsEntity.add(postEntity)
                }
                appDatabase.userDao().insertPostsById(allPostsEntity)
            } else {
                allPostsEntity = appDatabase.userDao().getPostsById(userId)
            }
        }
        return Resource.Success(allPostsEntity)
    }

    override suspend fun getUser(userId: Int): Resource.Success<UserEntity> {
        var userEntity: UserEntity
        withContext(Dispatchers.IO) {
            userEntity = appDatabase.userDao().getUserById(userId)
        }
        return Resource.Success(userEntity)
    }

}