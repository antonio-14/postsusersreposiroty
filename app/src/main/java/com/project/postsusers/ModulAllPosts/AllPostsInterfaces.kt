package com.project.postsusers.ModulAllPosts

import com.project.postsusers.Model.Room.PostEntity
import com.project.postsusers.Model.Room.UserEntity
import com.project.postsusers.Utils.vo.Resource

class AllPostsInterfaces {

    interface AllPostsDataSourceI {
        suspend fun getAllPostsByUser(userId: Int): Resource.Success<MutableList<PostEntity>>
        suspend fun getUser(userId: Int): Resource.Success<UserEntity>
    }

    interface AllPostsRepoI {
        suspend fun getAllPostsByUser(userId: Int): Resource.Success<MutableList<PostEntity>>
        suspend fun getUser(userId: Int): Resource.Success<UserEntity>
    }

    interface PostsInterface {

    }

}