package com.project.postsusers.ModulAllPosts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.project.postsusers.Utils.vo.Resource
import kotlinx.coroutines.Dispatchers

class AllPostsViewModel(private val repo: AllPostsRepo): ViewModel() {

    fun getAllPostsByUser(userId: Int) = liveData(viewModelScope.coroutineContext + Dispatchers.Main) {
        emit(Resource.Loading())
        try {
            emit(repo.getAllPostsByUser(userId))
        } catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }

    fun getUSer(userId: Int) = liveData(viewModelScope.coroutineContext + Dispatchers.Main) {
        emit(Resource.Loading())
        try {
            emit(repo.getUser(userId))
        } catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }

}

class AllPostsViewModelFactory(private val repo: AllPostsRepo) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(AllPostsRepo::class.java).newInstance(repo)
    }
}