package com.project.postsusers.Utils.Extensions

import com.project.postsusers.Model.Post
import com.project.postsusers.Model.Room.PostEntity
import com.project.postsusers.Model.Room.UserEntity
import com.project.postsusers.Model.User


fun User.mapperUserEntity() = UserEntity(id, name, userName, email, phone, website)

fun Post.mapperPostEntity() = PostEntity(id, userId, title, body)