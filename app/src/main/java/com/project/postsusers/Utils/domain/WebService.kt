package com.project.postsusers.Utils.domain

import com.project.postsusers.Model.Post
import com.project.postsusers.Model.User
import retrofit2.http.GET
import retrofit2.http.Query

interface WebService {

    @GET("users")
    suspend fun getAllUsers(): List<User>

    @GET("posts")
    suspend fun getAllPosts(): List<Post>

    @GET("posts")
    suspend fun getPostsForUserId(@Query("userId") userId: Int): List<Post>

}