package com.project.postsusers.Utils.domain

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.project.postsusers.Model.Room.PostEntity
import com.project.postsusers.Model.Room.UserEntity

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllUsers(users: List<UserEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllPosts(posts: List<PostEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPostsById(posts: List<PostEntity>)

    @Query("SELECT * FROM UserEntity")
    fun getAllUsers(): MutableList<UserEntity>

    @Query("SELECT * FROM PostEntity WHERE userId LIKE :postsId")
    fun getPostsById(postsId: Int): MutableList<PostEntity>

    @Query("SELECT * FROM UserEntity WHERE id LIKE :userId LIMIT 1")
    fun getUserById(userId: Int): UserEntity

}