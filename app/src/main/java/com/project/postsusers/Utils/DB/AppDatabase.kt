package com.project.postsusers.Utils.DB

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.project.postsusers.Model.Room.PostEntity
import com.project.postsusers.Model.Room.UserEntity
import com.project.postsusers.Utils.domain.UserDao

@Database(entities = [PostEntity::class, UserEntity::class], version = 1)
abstract class AppDatabase: RoomDatabase() {


    abstract fun userDao(): UserDao

    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            INSTANCE = INSTANCE?: Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "table_users").fallbackToDestructiveMigration().build()
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

}