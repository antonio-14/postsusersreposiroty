package com.project.postsusers.Utils.vo

import com.google.gson.GsonBuilder
import com.project.postsusers.Utils.Constants
import com.project.postsusers.Utils.domain.WebService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {

    val webService by lazy {
        Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build().create(WebService::class.java)
    }

}