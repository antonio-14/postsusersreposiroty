package com.project.postsusers.Model.Room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserEntity(
    @PrimaryKey
    var id: Int = 0,
    var name: String? = null,
    var userName: String? = null,
    var email: String? = null,
    var phone: String? = null,
    var website: String? = null
)
