package com.project.postsusers.Model.Room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PostEntity(
    @PrimaryKey
    var id: Int = 0,
    var userId: Int = 0,
    var title: String? = null,
    var body: String? = null
)
