package com.project.postsusers.Model

import com.google.gson.annotations.SerializedName

data class User(
    var id: Int = 0,
    var name: String? = null,
    @SerializedName("username")
    var userName: String? = null,
    var email: String? = null,
    var phone: String? = null,
    var website: String? = null
)